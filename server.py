from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import csv


# Define the HTTP request handler class
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    # Define the response headers
    def _set_headers(self, content_type):
        self.send_response(200)
        self.send_header("Content-type", content_type)
        self.end_headers()

    # Handle GET requests
    def do_GET(self):
        if self.path == "/":
            self._set_headers("text/html")
            self.wfile.write(b"<html><body><h1>Hello, World!</h1></body></html>")
        elif self.path == "/xml":
            self._set_headers("application/xml")
            self.wfile.write(b"<root><message>Hello, World!</message></root>")
        elif self.path == "/json":
            self._set_headers("application/json")
            data = {"message": "Hello, World!"}
            json_data = json.dumps(data).encode("utf-8")
            self.wfile.write(json_data)
        elif self.path == "/csv":
            self._set_headers("text/csv")
            csv_data = "message\nHello, World!\n"
            self.wfile.write(csv_data.encode("utf-8"))
        else:
            self._set_headers("text/html")
            self.wfile.write(b"<html><body><h1>Page not found</h1></body></html>")


# Create the HTTP server
def run(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler, port=8000):
    server_address = ("", port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting server on port {port}...")
    httpd.serve_forever()


# Run the server
if __name__ == "__main__":
    run()
