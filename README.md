# Simple Web Server in Python

This is a basic example of creating a simple web server using Python. The server responds to different HTTP requests with different types of content, such as HTML, XML, JSON, or CSV.

## Code Explanation

The code uses Python's built-in `http.server` module, which provides the necessary classes to implement an HTTP server.

1. First, we import the required modules:

   - `BaseHTTPRequestHandler` is the base class for handling HTTP requests.
   - `HTTPServer` is a simple HTTP server class.

2. Next, we define a custom request handler class by subclassing `BaseHTTPRequestHandler`. This class overrides the `do_GET()` method to handle GET requests.

3. Inside the `do_GET()` method, we check the requested path and respond accordingly:

   - If the path is `'/'`, we respond with a simple HTML message saying "Hello, World!".
   - If the path is `'/xml'`, we respond with an XML message wrapping "Hello, World!".
   - If the path is `'/json'`, we respond with a JSON message containing the key-value pair of "message" and "Hello, World!".
   - If the path is `'/csv'`, we respond with a CSV message containing the header "message" and the value "Hello, World!".
   - For any other path, we respond with an HTML message saying "Page not found".

4. The `_set_headers()` method is used to set the response headers based on the content type.

5. The `run()` function is responsible for creating the HTTP server, specifying the server address and port.

6. Finally, we check if the script is being run directly and call the `run()` function to start the server on port 8000.

## Running the Server

To run the server:

1. Save the code into a file named `server.py`.
2. Open a terminal and navigate to the directory containing `server.py`.
3. Run the command: `python server.py`.
4. The server will start running on `localhost` at port `8000`.
5. Open a web browser and visit `http://localhost:8000` to see the "Hello, World!" message in HTML format.
6. You can also visit `http://localhost:8000/xml`, `http://localhost:8000/json`, and `http://localhost:8000/csv` to see the respective content types.

Feel free to modify the code to add more routes or handle additional content types as needed.
